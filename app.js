/* Ответы на теоретические вопросы:
1. Новый тег на странице можно создать с помощью createElement() или insertAdjacentHtml()
2. insertAdjacentHTML: beforebegin (перед el)
                       afterbegin (в начало el) 
                       beforeend (в конец el)
                       afterend (после el)
3. Удалить el.remove() или присвоить пустую строку */


const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function getElem(arr, parent) {
    const body = document.body
    const ul = document.querySelector('#body');
    arr.forEach(el => {
        if (parent === undefined) {
            body.innerHTML += "<li>" + el + "</li>";
        } else if (parent === 'ul') {
            ul.innerHTML += "<li>" + el + "</li>"
        }
    });
}
getElem(arr)

function printNumbers(from, to) {
    let current = from;
    let timer = setInterval(() => {
        document.querySelector('.timer').innerHTML = current
        if (current === to) {
            clearInterval(timer);
            document.body.hidden = true
        }
        current--;
    }, 1000);
}
printNumbers(3, 0);




